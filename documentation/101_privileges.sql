INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
('unicaencalendrier_index', 'Paramétrage du module de gestion des calendrier', 'UnicaenCalendrier\Provider\Privilege', 6000);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'index', 'Accés à l''administration du module', 10
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'unicaencalendrier_index';

INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
('calendrier', 'Gestion des calendriers', 'UnicaenCalendrier\Provider\Privilege', 6200);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'calendrier_index', 'Accéder à l''index', 10 UNION
    SELECT 'calendrier_afficher', 'Afficher', 20 UNION
    SELECT 'calendrier_ajouter', 'Ajouter', 30 UNION
    SELECT 'calendrier_modifier', 'Modifier', 40 UNION
    SELECT 'calendrier_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'calendrier_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'calendrier';

INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
('calendriertype', 'Gestion des types de calendriers', 'UnicaenCalendrier\Provider\Privilege', 6100);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'calendriertype_index', 'Accéder à l''index', 10 UNION
    SELECT 'calendriertype_afficher', 'Afficher', 20 UNION
    SELECT 'calendriertype_ajouter', 'Ajouter', 30 UNION
    SELECT 'calendriertype_modifier', 'Modifier', 40 UNION
    SELECT 'calendriertype_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'calendriertype';

INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
('datetype', 'Gestion des types de dates', 'UnicaenCalendrier\Provider\Privilege', 6300);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'datetype_index', 'Accéder à l''index', 10 UNION
    SELECT 'datetype_afficher', 'Afficher', 20 UNION
    SELECT 'datetype_ajouter', 'Ajouter', 30 UNION
    SELECT 'datetype_modifier', 'Modifier', 40 UNION
    SELECT 'datetype_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'datetype';

INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
('date', 'Gestion des dates', 'UnicaenCalendrier\Provider\Privilege', 6400);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'date_index', 'Accéder à l''index', 10 UNION
    SELECT 'date_afficher', 'Afficher', 20 UNION
    SELECT 'date_ajouter', 'Ajouter', 30 UNION
    SELECT 'date_modifier', 'Modifier', 40 UNION
    SELECT 'date_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'date_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'date';
