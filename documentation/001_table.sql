create table unicaen_calendrier_date_type
(
    id             serial                not null
        constraint unicaen_calendrier_date_type_pk primary key,
    code           varchar(64)           not null
        constraint unicaen_calendrier_date_type_pk2 unique,
    libelle        varchar(1024)         not null,
    description    text,
    interval       boolean default false not null,
    duree_a_priori integer
);

create table unicaen_calendrier_calendrier_type
(
    id      serial      not null
        constraint unicaen_calendrier_calendrier_type_pk primary key,
    code    varchar(64) not null
        constraint unicaen_calendrier_calendrier_type_pk2 unique,
    libelle varchar(1024)
);

create table unicaen_calendrier_calendriertype_datetype
(
    calendrier_type_id integer not null
        constraint uc_calendriertype_datetype_calendrier_type_id_fk references unicaen_calendrier_calendrier_type on delete cascade,
    date_type_id       integer not null
        constraint uc_calendriertype_datetype_calendrier_date_type_id_fk references unicaen_calendrier_date_type on delete cascade,
    constraint unicaen_calendrier_calendriertype_datetype_pk primary key (calendrier_type_id, date_type_id)
);

create table unicaen_calendrier_calendrier
(
    id                    serial                  not null
        constraint unicaen_calendrier_calendrier_pk primary key,
    libelle               varchar(1024)           not null,
    calendrier_type_id integer not null
        constraint uc_calendriertype_calendrier_type_id_fk references unicaen_calendrier_calendrier_type on delete cascade;
    histo_creation        timestamp default now() not null,
    histo_createur_id     integer   default 0     not null
        constraint unicaen_calendrier_calendrier_unicaen_utilisateur_user_id_fk references unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_modificateur_id integer
        constraint unicaen_calendrier_calendrier_unicaen_utilisateur_user_id_fk2 references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint unicaen_calendrier_calendrier_unicaen_utilisateur_user_id_fk3 references unicaen_utilisateur_user
);

create table unicaen_calendrier_date
(
    id                    serial                  not null
        constraint unicaen_calendrier_date_pk primary key,
    date_type_id          integer                 not null
        constraint unicaen_calendrier_date_unicaen_calendrier_date_type_id_fk references unicaen_calendrier_date_type on delete cascade,
    debut                 timestamp,
    fin                   timestamp,
    histo_creation        timestamp default now() not null,
    histo_createur_id     integer   default 0     not null
        constraint unicaen_calendrier_date_unicaen_utilisateur_user_id_fk references unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_modificateur_id integer
        constraint unicaen_calendrier_date_unicaen_utilisateur_user_id_fk2 references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint unicaen_calendrier_date_unicaen_utilisateur_user_id_fk3 references unicaen_utilisateur_user
);

create table unicaen_calendrier_calendrier_date
(
    calendrier_id integer not null
        constraint uc_calendrier_date_unicaen_calendrier_calendrier_id_fk
            references unicaen_calendrier_calendrier
            on delete cascade,
    date_id       integer not null
        constraint uc_calendrier_date_unicaen_calendrier_date_id_fk
            references unicaen_calendrier_date
            on delete cascade,
    constraint unicaen_calendrier_calendrier_date_pk
        primary key (calendrier_id, date_id)
);



