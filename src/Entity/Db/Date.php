<?php

namespace UnicaenCalendrier\Entity\Db;

use DateTime;
use UnicaenCalendrier\Entity\HasCalendriersInterface;
use UnicaenCalendrier\Entity\HasCalendriersTrait;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Date implements HistoriqueAwareInterface, HasCalendriersInterface {
    use HistoriqueAwareTrait;
    use HasCalendriersTrait;

    const FORMAT = 'Y-m-d';

    private ?int $id = null;
    private ?DateType $type = null;
    private ?DateTime $debut = null;
    private ?DateTime $fin = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?DateType
    {
        return $this->type;
    }

    public function setType(?DateType $type): void
    {
        $this->type = $type;
    }

    public function getDebut(): ?DateTime
    {
        return $this->debut;
    }

    public function setDebut(?DateTime $debut): void
    {
        $this->debut = $debut;
    }

    public function getFin(): ?DateTime
    {
        return $this->fin;
    }

    public function setFin(?DateTime $fin): void
    {
        $this->fin = $fin;
    }

    public function estTerminee(DateTime $dateObs = null): bool
    {
        if(!isset($this->debut) && !isset($this->fin)){return false;}
        if (empty($dateObs)) {
            $dateObs = new DateTime();
        }
        $dFin = ($this->fin) ?? ($this->debut);
        return $dFin < $dateObs;
    }

    public function estEnCours(DateTime $dateObs = null): bool
    {
        if(!isset($this->debut) && !isset($this->fin)){return false;}
        if (empty($dateObs)) {
            $dateObs = new DateTime();
        }

        $dFin = ($this->fin) ?? ($dateObs);
        $dDebut = ($this->debut) ?? ($dateObs);

        return $dDebut <= $dateObs && $dateObs <= $dFin;
    }

    public function estFutur(DateTime $dateObs = null): bool
    {
        if(!isset($this->debut) && !isset($this->fin)){return false;}
        if (empty($dateObs)) {
            $dateObs = new DateTime();
        }
        $dDebut = ($this->debut) ?? ($this->fin);
        return  $dateObs < $dDebut;
    }

}