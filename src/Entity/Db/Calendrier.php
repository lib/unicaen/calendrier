<?php

namespace UnicaenCalendrier\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenCalendrier\Entity\HasCalendrierTypeInterface;
use UnicaenCalendrier\Entity\HasCalendrierTypeTrait;
use UnicaenCalendrier\Entity\HasDatesInterface;
use UnicaenCalendrier\Entity\HasDatesTrait;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Calendrier implements HistoriqueAwareInterface, HasCalendrierTypeInterface, HasDatesInterface
{
    use HistoriqueAwareTrait;
    use HasCalendrierTypeTrait;
    use HasDatesTrait;

    private ?int $id = null;
    private ?string $libelle = null;

    public function  __construct()
    {
        $this->dates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

}