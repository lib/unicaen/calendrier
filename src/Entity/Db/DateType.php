<?php

namespace UnicaenCalendrier\Entity\Db;

use Doctrine\Common\Collections\Collection;

class DateType {

    protected ?int $id=null;
    protected ?string $code=null;
    protected ?string $libelle=null;
    protected bool $interval = false;
    protected ?int $dureeApriori = null;
    protected ?string $description = null;
    protected Collection $datesPrecedentes; //todo ?precedent comme objet pour proter l'info de durée
    protected Collection $datesSuivantes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function isInterval(): bool
    {
        return $this->interval;
    }

    public function setInterval(bool $isIntervalle): void
    {
        $this->interval = $isIntervalle;
    }

    public function getDureeApriori(): ?int
    {
        return $this->dureeApriori;
    }

    public function setDureeApriori(?int $dureeApriori): void
    {
        $this->dureeApriori = $dureeApriori;
    }

    public function getDatesPrecedentes(): Collection
    {
        return $this->datesPrecedentes;
    }

    public function setDatesPrecedentes(Collection $datesPrecedentes): void
    {
        $this->datesPrecedentes = $datesPrecedentes;
    }

    public function getDatesSuivantes(): Collection
    {
        return $this->datesSuivantes;
    }

    public function setDatesSuivantes(Collection $datesSuivantes): void
    {
        $this->datesSuivantes = $datesSuivantes;
    }


}