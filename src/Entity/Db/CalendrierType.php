<?php

namespace UnicaenCalendrier\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use UnicaenCalendrier\Entity\HasDatesTypesInterface;
use UnicaenCalendrier\Entity\HasDatesTypesTrait;

class CalendrierType implements HasDatesTypesInterface
{
    use HasDatesTypesTrait;

    private ?int $id = null;
    private ?string $code = null;
    private ?string $libelle = null;

    public function __construct()
    {
        $this->datesTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

}