<?php

namespace UnicaenCalendrier\Entity;

use UnicaenCalendrier\Entity\Db\CalendrierType;

interface HasCalendrierTypeInterface {

    public function getCalendrierType(): ?CalendrierType;
    public function setCalendrierType(?CalendrierType $calendrierType): void;

}