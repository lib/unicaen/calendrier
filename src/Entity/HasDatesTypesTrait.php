<?php

namespace UnicaenCalendrier\Entity;

use Doctrine\Common\Collections\Collection;
use UnicaenCalendrier\Entity\Db\DateType;

trait HasDatesTypesTrait {

    private Collection $datesTypes;

    /** @return DateType[] */
    public function getDatesTypes(): array
    {
        return $this->datesTypes->toArray();
    }

    public function addDateType(DateType $dateType): void
    {
        $this->datesTypes->add($dateType);
    }

    public function removeDateType(DateType $dateType): void
    {
        $this->datesTypes->removeElement($dateType);
    }

    public function hasDateType(?DateType $dateType): bool
    {
        return $this->datesTypes->contains($dateType);
    }
}