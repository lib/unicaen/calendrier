<?php

namespace UnicaenCalendrier\Entity;

use UnicaenCalendrier\Entity\Db\DateType;

interface HasDatesTypesInterface {

    public function getDatesTypes(): array;
    public function addDateType(DateType $dateType): void;
    public function removeDateType(DateType $dateType): void;
    public function hasDateType(?DateType $dateType): bool;
}