<?php

namespace UnicaenCalendrier\Entity;

use Doctrine\Common\Collections\Collection;
use UnicaenCalendrier\Entity\Db\Date;

trait HasDatesTrait
{

    private Collection $dates;

    /** @return Date[] */
    public function getDates(): array
    {
        return $this->dates->toArray();
    }

    public function addDate(Date $date): void
    {
        $this->dates->add($date);
    }

    public function removeDate(Date $date): void
    {
        $this->dates->removeElement($date);
    }

    public function hasDate(?Date $date): bool
    {
        return $this->dates->contains($date);
    }
}