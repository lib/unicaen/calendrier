<?php

namespace UnicaenCalendrier\Entity;

use Doctrine\Common\Collections\Collection;
use UnicaenCalendrier\Entity\Db\Calendrier;

trait HasCalendriersTrait {

    private Collection $calendriers;

    /** @return Calendrier[] */
    public function getCalendriers() : array
    {
        return $this->calendriers->toArray();
    }

    public function addCalendrier(Calendrier $calendrier): void
    {
        $this->calendriers->add($calendrier);
    }

    public function removeCalendrier(Calendrier $calendrier): void
    {
        $this->calendriers->removeElement($calendrier);
    }

    public function hasCalendrier(?Calendrier $calendrier): bool
    {
        return $this->calendriers->contains($calendrier);
    }

    public function getCalendrierByTypeCode(string $code): ?Calendrier
    {
        /** @var Calendrier $calendrier */
        foreach ($this->calendriers as $calendrier) {
            if ($calendrier->estNonHistorise() && $calendrier->getCalendrierType()->getCode() === $code) return $calendrier;
        }
        return null;
    }
}