<?php

namespace UnicaenCalendrier\Entity;

use UnicaenCalendrier\Entity\Db\Date;

interface HasDatesInterface {

    public function getDates(): array;
    public function addDate(Date $date): void;
    public function removeDate(Date $date): void;
    public function hasDate(?Date $date): bool;
}