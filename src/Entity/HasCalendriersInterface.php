<?php

namespace UnicaenCalendrier\Entity;

use UnicaenCalendrier\Entity\Db\Calendrier;

interface HasCalendriersInterface {

    public function getCalendriers() : array;
    public function addCalendrier(Calendrier $calendrier): void;
    public function removeCalendrier(Calendrier $calendrier): void;
    public function hasCalendrier(?Calendrier $calendrier): bool;
    public function getCalendrierByTypeCode(string $code): ?Calendrier;
}