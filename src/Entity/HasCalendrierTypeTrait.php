<?php

namespace UnicaenCalendrier\Entity;

use UnicaenCalendrier\Entity\Db\CalendrierType;

trait HasCalendrierTypeTrait {

    private ?CalendrierType $calendrierType = null;

    public function getCalendrierType(): ?CalendrierType
    {
        return $this->calendrierType;
    }

    public function setCalendrierType(?CalendrierType $calendrierType): void
    {
        $this->calendrierType = $calendrierType;
    }
}