<?php

namespace UnicaenCalendrier\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenCalendrier\Entity\Db\CalendrierType;
use UnicaenCalendrier\Form\CalendrierType\CalendrierTypeFormAwareTrait;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesFormAwareTrait;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeServiceAwareTrait;
use UnicaenCalendrier\Service\DateType\DateTypeServiceAwareTrait;

class CalendrierTypeController extends AbstractActionController
{
    use CalendrierTypeServiceAwareTrait;
    use DateTypeServiceAwareTrait;
    use CalendrierTypeFormAwareTrait;
    use SelectionnerDatesTypesFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $calendriersType = $this->getCalendrierTypeService()->getCalendriersTypes();

        return new ViewModel([
            'calendriersTypes' => $calendriersType
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $calendrierType = $this->getCalendrierTypeService()->getRequestedCalendrierType($this);

        return new ViewModel([
            'calendrierType' => $calendrierType
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $calendrierType = new CalendrierType();

        $form = $this->getCalendrierTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/calendrier-type/ajouter', [], [], true));
        $form->bind($calendrierType);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCalendrierTypeService()->create($calendrierType);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter un type de calendrier",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $calendrierType = $this->getCalendrierTypeService()->getRequestedCalendrierType($this);

        $form = $this->getCalendrierTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/calendrier-type/modifier', ['calendrier-type' => $calendrierType->getId()], [], true));
        $form->bind($calendrierType);
        $form->setOldCode($calendrierType->getCode());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCalendrierTypeService()->update($calendrierType);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modifier un type de calendrier",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function supprimerAction() : ViewModel
    {
        $calendrierType = $this->getCalendrierTypeService()->getRequestedCalendrierType($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getCalendrierTypeService()->delete($calendrierType);
            exit();
        }

        $vm = new ViewModel();
        if ($calendrierType !== null) {
            $vm->setTemplate('unicaen-calendrier/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du type de date [" . $calendrierType->getCode() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-calendrier/calendrier-type/supprimer', ["calendrier-type" => $calendrierType->getId()], [], true),
            ]);
        }
        return $vm;
    }

    public function gererDatesTypesAction(): ViewModel
    {
        $calendrierType = $this->getCalendrierTypeService()->getRequestedCalendrierType($this);

        $form = $this->getSelectionDatesTypesForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/calendrier-type/gerer-dates-types', ["calendrier-type" => $calendrierType->getId()], [], true));
        $form->bind($calendrierType);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCalendrierTypeService()->update($calendrierType);
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter une date type à un calendrier type",
            'form' => $form
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

}