<?php

namespace UnicaenCalendrier\Controller;

use Psr\Container\ContainerInterface;
use UnicaenCalendrier\Form\Date\DateForm;
use UnicaenCalendrier\Service\Date\DateService;

class DateControllerFactory {

    public function __invoke(ContainerInterface $container): DateController
    {
        /**
         * @var DateService $dateService
         * @var DateForm $dateForm
         */
        $dateService = $container->get(DateService::class);
        $dateForm = $container->get('FormElementManager')->get(DateForm::class);

        $controller = new DateController();
        $controller->setDateService($dateService);
        $controller->setDateForm($dateForm);
        return $controller;
    }
}