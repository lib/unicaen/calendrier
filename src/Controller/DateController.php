<?php

namespace UnicaenCalendrier\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenCalendrier\Entity\Db\Date;
use UnicaenCalendrier\Form\Date\DateFormAwareTrait;
use UnicaenCalendrier\Service\Date\DateServiceAwareTrait;

class DateController extends AbstractActionController {
    use DateServiceAwareTrait;
    use DateFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $dates = $this->getDateService()->getDates('debut', 'ASC', true);

        return new ViewModel([
            'dates' => $dates,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $date = $this->getDateService()->getRequestedDate($this);

        return new ViewModel([
            'title' => "Affichage de la date [".$date->getId()."]",
            'date' => $date,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $date = new Date();
        $form = $this->getDateForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/date/ajouter', [], [], true));
        $form->bind($date);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getDateService()->create($date);
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter une date",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $date = $this->getDateService()->getRequestedDate($this);

        $form = $this->getDateForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/date/modifier', ['date' => $date->getId()], [], true));
        $form->bind($date);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getDateService()->update($date);
            }
        }

        $vm = new ViewModel([
            'title' => "Modifer la date [".$date->getId()."]",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $date =  $this->getDateService()->getRequestedDate($this);
        $this->getDateService()->historise($date);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-calendrier/date');
    }

    public function restaurerAction(): Response
    {
        $date =  $this->getDateService()->getRequestedDate($this);
        $this->getDateService()->restore($date);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-calendrier/date');
    }

    public function supprimerAction(): ViewModel
    {
        $date =  $this->getDateService()->getRequestedDate($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getDateService()->delete($date);
            exit();
        }

        $vm = new ViewModel();
        if ($date !== null) {
            $vm->setTemplate('unicaen-calendrier/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la date [" . $date->getId() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-calendrier/date/supprimer', ["date" => $date->getId()], [], true),
            ]);
        }
        return $vm;
    }
}