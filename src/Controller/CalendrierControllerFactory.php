<?php

namespace UnicaenCalendrier\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Form\Calendrier\CalendrierForm;
use UnicaenCalendrier\Form\Date\DateForm;
use UnicaenCalendrier\Form\ModifierLibelle\ModifierLibelleForm;
use UnicaenCalendrier\Service\Calendrier\CalendrierService;
use UnicaenCalendrier\Service\Date\DateService;

class CalendrierControllerFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CalendrierController
    {

        /**
         * @var CalendrierService $calendrierService
         * @var DateService $dateService
         * @var CalendrierForm $calendrierForm
         * @var DateForm $dateForm
         * @var ModifierLibelleForm $modifierLibelleForm
         */
        $calendrierService = $container->get(CalendrierService::class);
        $dateService = $container->get(DateService::class);
        $calendrierForm = $container->get('FormElementManager')->get(CalendrierForm::class);
        $dateForm = $container->get('FormElementManager')->get(DateForm::class);
        $modifierLibelleForm = $container->get('FormElementManager')->get(ModifierLibelleForm::class);

        $controller = new CalendrierController();
        $controller->setCalendrierService($calendrierService);
        $controller->setDateService($dateService);
        $controller->setCalendrierForm($calendrierForm);
        $controller->setDateForm($dateForm);
        $controller->setModifierLibelleForm($modifierLibelleForm);
        return $controller;
    }

}