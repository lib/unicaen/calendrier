<?php

namespace UnicaenCalendrier\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Form\DateType\DateTypeForm;
use UnicaenCalendrier\Service\DateType\DateTypeService;

class DateTypeControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): DateTypeController
    {
        /**
         * @var DateTypeService $dateTypeService
         * @var DateTypeForm $dateTypeForm
         */
        $dateTypeService = $container->get(DateTypeService::class);
        $dateTypeForm = $container->get('FormElementManager')->get(DateTypeForm::class);

        $controller = new DateTypeController();
        $controller->setDateTypeService($dateTypeService);
        $controller->setDateTypeForm($dateTypeForm);
        return $controller;
    }
}