<?php

namespace UnicaenCalendrier\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Form\CalendrierType\CalendrierTypeForm;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesForm;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeService;
use UnicaenCalendrier\Service\DateType\DateTypeService;

class CalendrierTypeControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CalendrierTypeController
    {
        /**
         * @var CalendrierTypeService $calendrierTypeService
         * @var DateTypeService $dateTypeService
         * @var CalendrierTypeForm $calendrierTypeForm
         * @var SelectionnerDatesTypesForm $selectionDatesTypesForm
         */
        $calendrierTypeService = $container->get(CalendrierTypeService::class);
        $dateTypeService = $container->get(DateTypeService::class);
        $calendrierTypeForm = $container->get('FormElementManager')->get(CalendrierTypeForm::class);
        $selectionDatesTypesForm = $container->get('FormElementManager')->get(SelectionnerDatesTypesForm::class);

        $controller = new CalendrierTypeController();
        $controller->setCalendrierTypeService($calendrierTypeService);
        $controller->setDateTypeService($dateTypeService);
        $controller->setCalendrierTypeForm($calendrierTypeForm);
        $controller->setSelectionDatesTypesForm($selectionDatesTypesForm);
        return $controller;
    }
}