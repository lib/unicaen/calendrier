<?php

namespace UnicaenCalendrier\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenCalendrier\Entity\Db\Calendrier;
use UnicaenCalendrier\Entity\Db\Date;
use UnicaenCalendrier\Entity\Db\DateType;
use UnicaenCalendrier\Form\Calendrier\CalendrierFormAwareTrait;
use UnicaenCalendrier\Form\Date\DateFormAwareTrait;
use UnicaenCalendrier\Form\ModifierLibelle\ModifierLibelleFormAwareTrait;
use UnicaenCalendrier\Service\Calendrier\CalendrierServiceAwareTrait;
use UnicaenCalendrier\Service\Date\DateServiceAwareTrait;

class CalendrierController extends AbstractActionController
{
    use CalendrierServiceAwareTrait;
    use DateServiceAwareTrait;
    use CalendrierFormAwareTrait;
    use DateFormAwareTrait;
    use ModifierLibelleFormAwareTrait;


    /** CRUD **********************************************************************************************************/

    public function indexAction(): ViewModel
    {
        $calendriers = $this->getCalendrierService()->getCalendriers('libelle', 'ASC', true);
        return new ViewModel([
            'calendriers' => $calendriers
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $calendrier = $this->getCalendrierService()->getRequestedCalendrier($this);

        return new ViewModel([
            'calendrier' => $calendrier
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $calendrier = new Calendrier();

        $form = $this->getCalendrierForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/calendrier/ajouter', [], [], true));
        $form->bind($calendrier);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCalendrierService()->create($calendrier);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter un calendrier",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $calendrier = $this->getCalendrierService()->getRequestedCalendrier($this);

        $form = $this->getCalendrierForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/calendrier/modifier', ['calendrier' => $calendrier->getId()], [], true));
        $form->bind($calendrier);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCalendrierService()->update($calendrier);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modifier un type de calendrier",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $calendrier = $this->getCalendrierService()->getRequestedCalendrier($this);
        $this->getCalendrierService()->historise($calendrier);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-calendrier/calendrier', [], [], true);
    }

    public function restaurerAction(): Response
    {
        $calendrier = $this->getCalendrierService()->getRequestedCalendrier($this);
        $this->getCalendrierService()->restore($calendrier);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-calendrier/calendrier', [], [], true);
    }

    public function supprimerAction(): ViewModel
    {
        $calendrier = $this->getCalendrierService()->getRequestedCalendrier($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getCalendrierService()->delete($calendrier);
            exit();
        }

        $vm = new ViewModel();
        if ($calendrier !== null) {
            $vm->setTemplate('unicaen-calendrier/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du calendrier [" . $calendrier->getLibelle() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-calendrier/calendrier/supprimer', ["calendrier" => $calendrier->getId()], [], true),
            ]);
        }
        return $vm;
    }

    public function modifierLibelleAction(): ViewModel
    {
        $calendrier = $this->getCalendrierService()->getRequestedCalendrier($this);

        $form = $this->getModifierLibelleForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/calendrier/modifier-libelle', ['calendrier' => $calendrier->getId()], [], true));
        $form->bind($calendrier);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getCalendrierService()->update($calendrier);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modification du libelle du calendrier",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    /** GESTION DES DATES *********************************************************************************************/

    /** Action pour ajouter directement une date à un calendrier diffère de l'action de base de date @see DateController::ajouterAction() */
    public function ajouterDateAction(): ViewModel
    {
        $calendrier = $this->getCalendrierService()->getRequestedCalendrier($this);

        $date = new Date();
        $form = $this->getDateForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/calendrier/ajouter-date', ['calendrier' => $calendrier->getId()], [], true));
        $form->bind($date);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getDateService()->create($date);
                $calendrier->addDate($date);
                $this->getCalendrierService()->update($calendrier);
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter une date au calendrier",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }
}