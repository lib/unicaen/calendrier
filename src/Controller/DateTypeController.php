<?php

namespace UnicaenCalendrier\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenCalendrier\Entity\Db\DateType;
use UnicaenCalendrier\Form\DateType\DateTypeFormAwareTrait;
use UnicaenCalendrier\Service\DateType\DateTypeServiceAwareTrait;

class DateTypeController extends AbstractActionController
{
    use DateTypeServiceAwareTrait;
    use DateTypeFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $datesTypes = $this->getDateTypeService()->getDatesTypes();

        return new ViewModel([
            'datesTypes' => $datesTypes
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $dateType = $this->getDateTypeService()->getRequestedDateType($this);

        return new ViewModel([
            'dateType' => $dateType
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $dateType = new DateType();

        $form = $this->getDateTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/date-type/ajouter', [], [], true));
        $form->bind($dateType);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getDateTypeService()->create($dateType);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter un type de date",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $dateType = $this->getDateTypeService()->getRequestedDateType($this);

        $form = $this->getDateTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-calendrier/date-type/modifier', ['date-type' => $dateType->getId()], [], true));
        $form->bind($dateType);
        $form->setOldCode($dateType->getCode());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getDateTypeService()->update($dateType);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modifier un type de date",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-calendrier/default/default-form');
        return $vm;
    }

    public function supprimerAction() : ViewModel
    {
        $dateType = $this->getDateTypeService()->getRequestedDateType($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getDateTypeService()->delete($dateType);
            exit();
        }

        $vm = new ViewModel();
        if ($dateType !== null) {
            $vm->setTemplate('unicaen-calendrier/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du type de date [" . $dateType->getCode() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-calendrier/date-type/supprimer', ["date-type" => $dateType->getId()], [], true),
            ]);
        }
        return $vm;
    }
}