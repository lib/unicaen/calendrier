<?php

namespace UnicaenCalendrier\Form\Calendrier;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeService;

class CalendrierFormFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CalendrierForm
    {
        /**
         * @var EntityManager $entityManger
         * @var CalendrierTypeService $calendrierTypeService
         * @var CalendrierHydrator $hydrator
         */
        $entityManger = $container->get('doctrine.entitymanager.orm_default');
        $calendrierTypeService = $container->get(CalendrierTypeService::class);
        $hydrator = $container->get('HydratorManager')->get(CalendrierHydrator::class);

        $form = new CalendrierForm();
        $form->setObjectManager($entityManger);
        $form->setCalendrierTypeService($calendrierTypeService);
        $form->setHydrator($hydrator);
        return $form;
    }

}