<?php

namespace UnicaenCalendrier\Form\Calendrier;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeService;

class CalendrierHydratorFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CalendrierHydrator
    {
        /**
         * @var CalendrierTypeService $calendrierTypeService
         */
        $calendrierTypeService = $container->get(CalendrierTypeService::class);
        $hydrator = new CalendrierHydrator();
        $hydrator->setCalendrierTypeService($calendrierTypeService);
        return $hydrator;
    }

}