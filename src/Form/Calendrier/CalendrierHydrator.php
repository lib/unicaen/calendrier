<?php

namespace UnicaenCalendrier\Form\Calendrier;

use Laminas\Hydrator\HydratorInterface;
use UnicaenCalendrier\Entity\Db\Calendrier;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeServiceAwareTrait;

class CalendrierHydrator implements HydratorInterface {
    use CalendrierTypeServiceAwareTrait;

    public function extract(object $object): array
    {
        /** @var Calendrier $object */
        $data = [
            'libelle' => $object->getLibelle(),
            'calendrier-type' => $object->getCalendrierType()?->getId(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $libelle = (isset($data['libelle']) && trim($data['libelle']) !== '')?trim($data['libelle']):null;
        $type = (isset($data['calendrier-type']))?$this->getCalendrierTypeService()->getCalendrierType($data['calendrier-type']):null;

        /** @var Calendrier $object */
        $object->setLibelle($libelle);
        $object->setCalendrierType($type);
        return $object;
    }


}