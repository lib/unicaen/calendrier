<?php

namespace UnicaenCalendrier\Form\Calendrier;

use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeServiceAwareTrait;

class CalendrierForm extends Form
{
    use ProvidesObjectManager;
    use CalendrierTypeServiceAwareTrait;

    public function init(): void
    {
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé <span class='icon icon-obligatoire' title='Champ obligatoire'></span> : ",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //calendrier type
        $this->add([
            'type' => Select::class,
            'name' => 'calendrier-type',
            'options' => [
                'label' => "Type de calendrier <span class='icon icon-obligatoire'></span> : ",
                'label_options' => ['disable_html_escape' => true],
                'empty_option' => "Sélectionner un type de calendrier ...",
                'value_options' => $this->getCalendrierTypeService()->getCalendriersTypesAsOptions(),
            ],
            'attributes' => [
                'id' => 'calendrier-type',
                'data-live-search' => true,
                'class' => 'bootstrap-selectpicker show-tick',
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'submit',
            'options' => [
                'label' => "<span class='icon icon-sauvegarder'></span> Enregistrer",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'id' => 'submit'
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => ['required' => true,],
            'calendrier-type' => ['required' => true,],
        ]));
    }

}