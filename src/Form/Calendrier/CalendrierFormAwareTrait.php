<?php

namespace UnicaenCalendrier\Form\Calendrier;

trait CalendrierFormAwareTrait {

    private CalendrierForm $calendrierForm;

    public function getCalendrierForm(): CalendrierForm
    {
        return $this->calendrierForm;
    }

    public function setCalendrierForm(CalendrierForm $calendrierForm): void
    {
        $this->calendrierForm = $calendrierForm;
    }

}