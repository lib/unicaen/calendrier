<?php

namespace UnicaenCalendrier\Form\ModifierLibelle;

use Laminas\Hydrator\HydratorInterface;

class ModifierLibelleHydrator implements HydratorInterface
{

    public function extract(object $object): array
    {
        $data = [
            'libelle' => $object->getLibelle(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $libelle = (isset($data['libelle']) and trim($data['libelle']) !== '') ? trim($data['libelle']) : null;

        $object->setLibelle($libelle);
        return $object;
    }


}