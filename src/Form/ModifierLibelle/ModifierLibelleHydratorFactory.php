<?php

namespace UnicaenCalendrier\Form\ModifierLibelle;

use Psr\Container\ContainerInterface;

class ModifierLibelleHydratorFactory
{

    public function __invoke(ContainerInterface $container): ModifierLibelleHydrator
    {
        $hydrator = new ModifierLibelleHydrator();
        return $hydrator;
    }
}