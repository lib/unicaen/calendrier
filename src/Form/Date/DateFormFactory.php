<?php

namespace UnicaenCalendrier\Form\Date;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Service\DateType\DateTypeService;

class DateFormFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): DateForm
    {
        /**
         * @var DateTypeService $dateTypeService
         * @var DateHydrator $hydrator
         */
        $dateTypeService = $container->get(DateTypeService::class);
        $hydrator = $container->get('HydratorManager')->get(DateHydrator::class);

        $form = new DateForm();
        $form->setDateTypeService($dateTypeService);
        $form->setHydrator($hydrator);
        return $form;
    }
}