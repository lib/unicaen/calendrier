<?php

namespace UnicaenCalendrier\Form\Date;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Date;
use Laminas\Form\Element\Select;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use UnicaenCalendrier\Service\DateType\DateTypeServiceAwareTrait;

class DateForm extends Form {
    use DateTypeServiceAwareTrait;

    public function init(): void
    {
        //Select date type
        $this->add([
            'type' => Select::class,
            'name' => 'date-type',
            'options' => [
                'label' => "Type de date <span class='icon icon-obligatoire'></span> : ",
                'label_options' => [ 'disable_html_escape' => true ],
                'empty_option' => "Sélectionner un type de date ...",
                'value_options' => $this->getDateTypeService()->getDatesTypesAsOptions(),
            ],
            'attributes' => [
                'id' => 'date-type',
                'data-live-search' => true,
                'class' => 'bootstrap-selectpicker show-tick',
            ],
        ]);
        //Date début
        $this->add([
            'type'       => Date::class,
            'name'       => 'date-debut',
            'options'    => [
                'label'         => "Date (ou date de début) :",
            ],
            'attributes' => [
                'id' => 'date-debut',
            ],
        ]);
        //Date fin
        $this->add([
            'type'       => Date::class,
            'name'       => 'date-fin',
            'options'    => [
                'label'         => "Date de fin :",
            ],
            'attributes' => [
                'id' => 'date-fin',
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'submit',
            'options' => [
                'label' => "<span class='icon icon-sauvegarder'></span> Enregistrer",
                'label_options' => [ 'disable_html_escape' => true ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'id' => 'submit'
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'date-type' => [ 'required' => true, ],
            'date-debut' => [ 'required' => true, ],
            'date-fin' => [
//                'validators' => [[
//                    'name' => Callback::class,
//                    'options' => [
//                        'messages' => [
//                            Callback::INVALID_VALUE => "Une date de fin est attendue",
//                        ],
//                        'callback' => function ($value, $context = []) {
//                            var_dump($value);
//                            var_dump($context);
//                            die('mofo');
//                            return (isset($context['date-fin']) || !$this->getDateTypeService()->isInterval($context['date-type']));
//                        },
//                        //'break_chain_on_failure' => true,
//                    ],
//                ]],
                'required' => false,
            ],
        ]));
    }
}