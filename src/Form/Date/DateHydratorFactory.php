<?php

namespace UnicaenCalendrier\Form\Date;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Service\DateType\DateTypeService;

class DateHydratorFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : DateHydrator
    {
        /**
         * @var DateTypeService $dateTypeService
         */
        $dateTypeService = $container->get(DateTypeService::class);

        $hydrator = new DateHydrator();
        $hydrator->setDateTypeService($dateTypeService);
        return $hydrator;
    }
}