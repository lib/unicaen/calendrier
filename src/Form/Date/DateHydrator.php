<?php

namespace UnicaenCalendrier\Form\Date;

use DateTime;
use Laminas\Hydrator\HydratorInterface;
use UnicaenCalendrier\Entity\Db\Date;
use UnicaenCalendrier\Entity\Db\DateType;
use UnicaenCalendrier\Service\DateType\DateTypeServiceAwareTrait;

class DateHydrator implements HydratorInterface
{
    use DateTypeServiceAwareTrait;

    public function extract(object $object): array
    {
        /** @var Date $object */
        $data = [
            'date-type' => ($object->getType())?$object->getType()->getId():null,
            'date-debut' => ($object->getDebut())?$object->getDebut()->format(Date::FORMAT):null,
            'date-fin' => ($object->getFin())?$object->getFin()->format(Date::FORMAT):null,
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $type  = (isset($data['date-type']))?$this->getDateTypeService()->getDateType($data['date-type']):null;
        $debut = (isset($data['date-debut']))?DateTime::createFromFormat(Date::FORMAT, $data['date-debut']):null;
        $fin   = (isset($data['date-fin']))?DateTime::createFromFormat(Date::FORMAT, $data['date-fin']):null;

        /** @var Date $object */
        $object->setType($type);
        $object->setDebut($debut?:null);
        $object->setFin($fin?:null);
        return $object;
    }


}