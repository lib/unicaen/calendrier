<?php

namespace UnicaenCalendrier\Form\Date;

trait DateFormAwareTrait {

    private DateForm $dateForm;

    public function getDateForm(): DateForm
    {
        return $this->dateForm;
    }

    public function setDateForm(DateForm $dateForm): void
    {
        $this->dateForm = $dateForm;
    }

}