<?php

namespace UnicaenCalendrier\Form\DateType;

trait DateTypeFormAwareTrait {

    private DateTypeForm $dateTypeForm;

    public function getDateTypeForm(): DateTypeForm
    {
        return $this->dateTypeForm;
    }

    public function setDateTypeForm(DateTypeForm $dateTypeForm): void
    {
        $this->dateTypeForm = $dateTypeForm;
    }

}