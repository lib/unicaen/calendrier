<?php

namespace UnicaenCalendrier\Form\DateType;

use Psr\Container\ContainerInterface;

class DateTypeHydratorFactory
{
    public function __invoke(ContainerInterface $container): DateTypeHydrator
    {
        $hydrator = new DateTypeHydrator();
        return $hydrator;
    }

}