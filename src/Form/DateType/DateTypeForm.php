<?php

namespace UnicaenCalendrier\Form\DateType;

use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use UnicaenCalendrier\Entity\Db\DateType;

class DateTypeForm extends Form
{
    use ProvidesObjectManager;

    public function init(): void
    {
        //code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code <span class='icon icon-obligatoire' title='Champ obligatoire et unique'></span> : ",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        $this->add([
            'name' => 'old-code',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé <span class='icon icon-obligatoire' title='Champ obligatoire'></span> : ",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'description',
            'options' => [
                'label' => "Description : ",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'description',
            ],
        ]);
        //Interval
        $this->add([
            'type' => Select::class,
            'name' => 'interval',
            'options' => [
                'label' => "Est une période : ",
                'value_options' => [
                    false => "Non",
                    true => "Oui",
                ],
            ],
            'attributes' => [
                'id' => 'interval',
                'class' => 'bootstrap-selectpicker show-tick',
            ],
        ]);
        //duree
        $this->add([
            'type' => Number::class,
            'name' => 'duree',
            'options' => [
                'label' => "Durée à priori : ",
            ],
            'attributes' => [
                'id' => 'duree',
                'min' => 0,
                'max' => 365,
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'submit',
            'options' => [
                'label' => "<span class='icon icon-sauvegarder'></span> Enregistrer",
                'label_options' => [ 'disable_html_escape' => true ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'id' => 'submit'
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'code' => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà",
                        ],
                        'callback' => function ($value, $context = []) {
                            if ($value == $context['old-code']) return true;
                            return ($this->getObjectManager()->getRepository(DateType::class)->findOneBy(['code' => $value], []) == null);
                        },
                        //'break_chain_on_failure' => true,
                    ],
                ]],
            ],
            'old-code' => ['required' => false,],
            'libelle' => ['required' => true,],
            'description' => ['required' => false,],
            'interval' => ['required' => true,],
            'duree' => ['required' => false,],
        ]));
    }

    public function setOldCode(?string $value) : void
    {
        $this->get('old-code')->setValue($value);
    }

}