<?php

namespace UnicaenCalendrier\Form\DateType;

use Laminas\Hydrator\HydratorInterface;
use UnicaenCalendrier\Entity\Db\DateType;

class DateTypeHydrator implements HydratorInterface {

    public function extract(object $object): array
    {
        /** @var DateType $object */
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'interval' => $object->isInterval(),
            'duree' => $object->getDureeApriori(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $code = (isset($data['code']) && trim($data['code']) !== '')?trim($data['code']):null;
        $libelle = (isset($data['libelle']) && trim($data['libelle']) !== '')?trim($data['libelle']):null;
        $description = (isset($data['description']) && trim($data['description']) !== '')?trim($data['description']):null;
        $interval = $data['interval']??false;
        $duree = (isset($data['duree']) && trim($data['duree']) !== '')?((int) $data['duree']):null;

        /** @var DateType $object */
        $object->setCode($code);
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setInterval($interval);
        $object->setDureeApriori($duree);
        return $object;
    }


}