<?php

namespace UnicaenCalendrier\Form\DateType;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class DateTypeFormFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): DateTypeForm
    {
        /**
         * @var EntityManager $entityManger
         * @var DateTypeHydrator $hydrator
         */
        $entityManger = $container->get('doctrine.entitymanager.orm_default');
        $hydrator = $container->get('HydratorManager')->get(DateTypeHydrator::class);

        $form = new DateTypeForm();
        $form->setObjectManager($entityManger);
        $form->setHydrator($hydrator);
        return $form;
    }

}