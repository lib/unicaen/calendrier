<?php

namespace UnicaenCalendrier\Form\CalendrierType;

use Laminas\Hydrator\HydratorInterface;
use UnicaenCalendrier\Entity\Db\CalendrierType;

class CalendrierTypeHydrator implements HydratorInterface
{

    public function extract(object $object): array
    {
        /** @var CalendrierType $object */
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $code = (isset($data['code']) && trim($data['code']) !== '') ? trim($data['code']) : null;
        $libelle = (isset($data['libelle']) && trim($data['libelle']) !== '') ? trim($data['libelle']) : null;

        /** @var CalendrierType $object */
        $object->setCode($code);
        $object->setLibelle($libelle);
        return $object;
    }


}