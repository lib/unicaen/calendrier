<?php

namespace UnicaenCalendrier\Form\CalendrierType;

trait CalendrierTypeFormAwareTrait
{

    private CalendrierTypeForm $calendrierTypeForm;

    public function getCalendrierTypeForm(): CalendrierTypeForm
    {
        return $this->calendrierTypeForm;
    }

    public function setCalendrierTypeForm(CalendrierTypeForm $calendrierTypeForm): void
    {
        $this->calendrierTypeForm = $calendrierTypeForm;
    }

}