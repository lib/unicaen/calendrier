<?php

namespace UnicaenCalendrier\Form\CalendrierType;

use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use UnicaenCalendrier\Entity\Db\DateType;

class CalendrierTypeForm extends Form
{
    use ProvidesObjectManager;

    public function init(): void
    {
        //code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code <span class='icon icon-obligatoire' title='Champ obligatoire et unique'></span> : ",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        $this->add([
            'name' => 'old-code',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé <span class='icon icon-obligatoire' title='Champ obligatoire'></span> : ",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'submit',
            'options' => [
                'label' => "<span class='icon icon-sauvegarder'></span> Enregistrer",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'id' => 'submit'
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'code' => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà",
                        ],
                        'callback' => function ($value, $context = []) {
                            if ($value == $context['old-code']) return true;
                            return ($this->getObjectManager()->getRepository(DateType::class)->findOneBy(['code' => $value], []) == null);
                        },
                        //'break_chain_on_failure' => true,
                    ],
                ]],
            ],
            'old-code' => ['required' => false,],
            'libelle' => ['required' => true,],
        ]));
    }

    public function setOldCode(?string $value): void
    {
        $this->get('old-code')->setValue($value);
    }

}