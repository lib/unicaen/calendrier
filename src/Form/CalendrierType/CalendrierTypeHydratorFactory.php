<?php

namespace UnicaenCalendrier\Form\CalendrierType;

use Psr\Container\ContainerInterface;

class CalendrierTypeHydratorFactory
{
    public function __invoke(ContainerInterface $container): CalendrierTypeHydrator
    {
        $hydrator = new CalendrierTypeHydrator();
        return $hydrator;
    }
}