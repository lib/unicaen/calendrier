<?php

namespace UnicaenCalendrier\Form\CalendrierType;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class CalendrierTypeFormFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CalendrierTypeForm
    {
        /**
         * @var EntityManager $entityManger
         * @var CalendrierTypeHydrator $hydrator
         */
        $entityManger = $container->get('doctrine.entitymanager.orm_default');
        $hydrator = $container->get('HydratorManager')->get(CalendrierTypeHydrator::class);

        $form = new CalendrierTypeForm();
        $form->setObjectManager($entityManger);
        $form->setHydrator($hydrator);
        return $form;
    }

}