<?php

namespace UnicaenCalendrier\Form\SelectionnerDatesTypes;

trait SelectionnerDatesTypesFormAwareTrait {

    private SelectionnerDatesTypesForm $selectionDatesTypesForm;

    public function getSelectionDatesTypesForm(): SelectionnerDatesTypesForm
    {
        return $this->selectionDatesTypesForm;
    }

    public function setSelectionDatesTypesForm(SelectionnerDatesTypesForm $selectionDatesTypesForm): void
    {
        $this->selectionDatesTypesForm = $selectionDatesTypesForm;
    }

}