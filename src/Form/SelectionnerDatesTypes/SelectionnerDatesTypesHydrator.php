<?php

namespace UnicaenCalendrier\Form\SelectionnerDatesTypes;

use Laminas\Hydrator\HydratorInterface;
use UnicaenCalendrier\Entity\Db\DateType;
use UnicaenCalendrier\Entity\HasDatesTypesInterface;
use UnicaenCalendrier\Service\DateType\DateTypeServiceAwareTrait;

class SelectionnerDatesTypesHydrator implements HydratorInterface
{
    use DateTypeServiceAwareTrait;

    public function extract(object $object): array
    {
        /** @var HasDatesTypesInterface $object */
        $array = array_map(function (DateType $a) { return $a->getId(); }, $object->getDatesTypes());
        $data = [
            'dates-types' => $array
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {

        $types = $data['dates-types']??[];

        /** @var HasDatesTypesInterface $object */
        foreach ($object->getDatesTypes() as $type) {
            if (!in_array($type->getId(), $types)) {
                $object->removeDateType($type);
            }
        }
        foreach ($types as $id) {
            $type = $this->getDateTypeService()->getDateType($id);
            //addDateType est en charge de verifier qu'il n'y ait pas de doublon
            $object->addDateType($type);
        }
        return $object;

    }

}