<?php

namespace UnicaenCalendrier\Form\SelectionnerDatesTypes;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesForm;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesHydrator;
use UnicaenCalendrier\Service\DateType\DateTypeService;

class SelectionnerDatesTypesFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return SelectionnerDatesTypesForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionnerDatesTypesForm
    {
        /**
         * @var DateTypeService $dateTypeService
         * @var SelectionnerDatesTypesHydrator $hydrator;
         */
        $dateTypeService = $container->get(DateTypeService::class);
        $hydrator = $container->get('HydratorManager')->get(SelectionnerDatesTypesHydrator::class);

        $form = new SelectionnerDatesTypesForm();
        $form->setDateTypeService($dateTypeService);
        $form->setHydrator($hydrator);
        return $form;
    }
}