<?php

namespace UnicaenCalendrier\Form\SelectionnerDatesTypes;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenCalendrier\Service\DateType\DateTypeServiceAwareTrait;

class SelectionnerDatesTypesForm extends Form {
    use DateTypeServiceAwareTrait;

    public function init(): void
    {
        // types
        $this->add([
            'type' => Select::class,
            'name' => 'dates-types',
            'options' => [
                'label' => "Type de date <span class='icon icon-obligatoire'></span> : ",
                'label_options' => [ 'disable_html_escape' => true ],
                'empty_option' => "Sélectionner un type de date ...",
                'value_options' => $this->getDateTypeService()->getDatesTypesAsOptions(),
            ],
            'attributes' => [
                'id' => 'date-type',
                'data-live-search' => true,
                'multiple' => 'multiple',
                'class' => 'bootstrap-selectpicker show-tick',
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'submit',
            'options' => [
                'label' => "<span class='icon icon-sauvegarder'></span> Enregistrer",
                'label_options' => [ 'disable_html_escape' => true ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
                'id' => 'submit'
            ],
        ]);

        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'dates-types'           => [ 'required' => true, ],
        ]));
    }
}