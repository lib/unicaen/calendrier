<?php

namespace UnicaenCalendrier\Form\SelectionnerDatesTypes;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenCalendrier\Service\DateType\DateTypeService;

class SelectionnerDatesTypesHydratorFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionnerDatesTypesHydrator
    {
        /**
         * @var DateTypeService $dateTypeService
         */
        $dateTypeService = $container->get(DateTypeService::class);

        $hydrator = new SelectionnerDatesTypesHydrator();
        $hydrator->setDateTypeService($dateTypeService);
        return $hydrator;
    }
}