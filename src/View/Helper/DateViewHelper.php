<?php

namespace UnicaenCalendrier\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenCalendrier\Entity\Db\Calendrier;
use UnicaenCalendrier\Entity\Db\Date;

class DateViewHelper extends AbstractHelper
{
    /**
     * options :
     * - showInfo (default: false)
     * - showDate (default: true)
     * - showAction (default: false)
     */
    public function __invoke(Date $date, Calendrier $calendrier, array $options = []): string|Partial
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('date', ['date' => $date, 'calendrier' => $calendrier, 'options' => $options]);
    }
}