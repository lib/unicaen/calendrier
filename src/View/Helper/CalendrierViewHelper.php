<?php

namespace UnicaenCalendrier\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenCalendrier\Entity\Db\Calendrier;

class CalendrierViewHelper extends AbstractHelper
{
    public function __invoke(Calendrier $calendrier, array $options = []): string|Partial
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('calendrier', ['calendrier' => $calendrier, 'options' => $options]);
    }
}