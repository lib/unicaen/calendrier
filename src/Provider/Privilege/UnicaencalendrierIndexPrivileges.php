<?php

namespace UnicaenCalendrier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaencalendrierIndexPrivileges extends Privileges
{
    const INDEX = 'unicaencalendrier_index-index';
}