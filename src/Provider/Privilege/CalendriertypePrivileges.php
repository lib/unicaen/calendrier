<?php

namespace UnicaenCalendrier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class CalendriertypePrivileges extends Privileges
{
    const CALENDRIERTYPE_INDEX = 'calendriertype-calendriertype_index';
    const CALENDRIERTYPE_AFFICHER = 'calendriertype-calendriertype_afficher';
    const CALENDRIERTYPE_AJOUTER = 'calendriertype-calendriertype_ajouter';
    const CALENDRIERTYPE_MODIFIER = 'calendriertype-calendriertype_modifier';
    const CALENDRIERTYPE_SUPPRIMER = 'calendriertype-calendriertype_supprimer';
}