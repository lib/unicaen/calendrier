<?php

namespace UnicaenCalendrier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class DatePrivileges extends Privileges
{
    const DATE_INDEX = 'date-date_index';
    const DATE_AFFICHER = 'date-date_afficher';
    const DATE_AJOUTER = 'date-date_ajouter';
    const DATE_MODIFIER = 'date-date_modifier';
    const DATE_HISTORISER = 'date-date_historiser';
    const DATE_SUPPRIMER = 'date-date_supprimer';
}
