<?php

namespace UnicaenCalendrier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class DatetypePrivileges extends Privileges
{
    const DATETYPE_INDEX = 'datetype-datetype_index';
    const DATETYPE_AFFICHER = 'datetype-datetype_afficher';
    const DATETYPE_AJOUTER = 'datetype-datetype_ajouter';
    const DATETYPE_MODIFIER = 'datetype-datetype_modifier';
    const DATETYPE_SUPPRIMER = 'datetype-datetype_supprimer';
}