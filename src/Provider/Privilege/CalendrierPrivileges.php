<?php

namespace UnicaenCalendrier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class CalendrierPrivileges extends Privileges
{
    const CALENDRIER_INDEX = 'calendrier-calendrier_index';
    const CALENDRIER_AFFICHER = 'calendrier-calendrier_afficher';
    const CALENDRIER_AJOUTER = 'calendrier-calendrier_ajouter';
    const CALENDRIER_MODIFIER = 'calendrier-calendrier_modifier';
    const CALENDRIER_HISTORISER = 'calendrier-calendrier_historiser';
    const CALENDRIER_SUPPRIMER = 'calendrier-calendrier_supprimer';
}
