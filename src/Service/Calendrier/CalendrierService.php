<?php

namespace UnicaenCalendrier\Service\Calendrier;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenCalendrier\Entity\Db\Calendrier;

class CalendrierService {
    use ProvidesObjectManager;

    /** Gestion des entités *******************************************************************************************/

    public function create(Calendrier $calendrier): Calendrier
    {
        $this->getObjectManager()->persist($calendrier);
        $this->getObjectManager()->flush($calendrier);
        return $calendrier;
    }

    public function update(Calendrier $calendrier): Calendrier
    {
        $this->getObjectManager()->flush($calendrier);
        return $calendrier;
    }

    public function historise(Calendrier $calendrier): Calendrier
    {
        $calendrier->historiser();
        $this->getObjectManager()->flush($calendrier);
        return $calendrier;
    }

    public function restore(Calendrier $calendrier): Calendrier
    {
        $calendrier->dehistoriser();
        $this->getObjectManager()->flush($calendrier);
        return $calendrier;
    }

    public function delete(Calendrier $calendrier): Calendrier
    {
        $this->getObjectManager()->remove($calendrier);
        $this->getObjectManager()->flush($calendrier);
        return $calendrier;
    }

    /** Requetage *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Calendrier::class)->createQueryBuilder('calendrier')
            ->leftJoin('calendrier.dates','date')->addSelect('date')
            ->leftJoin('date.type','dtype')->addSelect('dtype')
        ;
        return $qb;
    }

    /** @return Calendrier[] */
    public function getCalendriers(string $champ='histoCreation', string $ordre='ASC', bool $withHisto = false): array
    {
        $qb= $this->createQueryBuilder()
            ->orderBy('calendrier.' . $champ, $ordre);
        if (!$withHisto) $qb = $qb->andWhere('calendrier.histoDestruction IS NULL');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getCalendrier(?int $id): ?Calendrier
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('calendrier.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".Calendrier::class."] partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    public function getRequestedCalendrier(AbstractActionController $controller, string $param='calendrier'): ?Calendrier
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getCalendrier($id);
    }

    /** Facade ********************************************************************************************************/


}