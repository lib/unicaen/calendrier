<?php

namespace UnicaenCalendrier\Service\Calendrier;

trait CalendrierServiceAwareTrait {

    private CalendrierService $calendrierService;

    public function getCalendrierService(): CalendrierService
    {
        return $this->calendrierService;
    }

    public function setCalendrierService(CalendrierService $calendrierService): void
    {
        $this->calendrierService = $calendrierService;
    }

}