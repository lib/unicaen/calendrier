<?php

namespace UnicaenCalendrier\Service\DateType;

trait DateTypeServiceAwareTrait {

    private DateTypeService $dateTypeService;

    public function getDateTypeService(): DateTypeService
    {
        return $this->dateTypeService;
    }

    public function setDateTypeService(DateTypeService $dateTypeService): void
    {
        $this->dateTypeService = $dateTypeService;
    }

}