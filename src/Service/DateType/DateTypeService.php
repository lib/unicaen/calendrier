<?php

namespace UnicaenCalendrier\Service\DateType;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenCalendrier\Entity\Db\DateType;

class DateTypeService {
    use ProvidesObjectManager;

    /** Gestion des entités *******************************************************************************************/

    public function create(DateType $dateType): DateType
    {
        $this->getObjectManager()->persist($dateType);
        $this->getObjectManager()->flush($dateType);
        return $dateType;
    }

    public function update(DateType $dateType): DateType
    {
        $this->getObjectManager()->flush($dateType);
        return $dateType;
    }

    public function delete(DateType $dateType): DateType
    {
        $this->getObjectManager()->remove($dateType);
        $this->getObjectManager()->flush($dateType);
        return $dateType;
    }

    /** Requetage *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(DateType::class)->createQueryBuilder('datetype')
        ;
        return $qb;
    }

    /** @return DateType[] */
    public function getDatesTypes(string $champ='libelle', string $ordre='ASC'): array
    {
        $qb= $this->createQueryBuilder()
            ->orderBy('datetype.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getDateType(?int $id): ?DateType
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('datetype.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".DateType::class."] partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    public function getRequestedDateType(AbstractActionController $controller, string $param='date-type'): ?DateType
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getDateType($id);
    }

    /** @return string[] */
    public function getDatesTypesAsOptions(): array
    {
        $types = $this->getDatesTypes();
        $options = [];
        foreach ($types as $type) {
            $options[$type->getId()] = $type->getLibelle();
        }
        return $options;
    }

    public function isInterval(?int $id): bool
    {
        $dateType = $this->getDateType($id);
        if ($dateType) return $dateType->isInterval();
        return false;
    }

    /** Facade ********************************************************************************************************/


}