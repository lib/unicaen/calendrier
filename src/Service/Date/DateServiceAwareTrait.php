<?php

namespace UnicaenCalendrier\Service\Date;

trait DateServiceAwareTrait {

    private DateService $dateService;

    public function getDateService(): DateService
    {
        return $this->dateService;
    }

    public function setDateService(DateService $dateService): void
    {
        $this->dateService = $dateService;
    }


}