<?php

namespace UnicaenCalendrier\Service\Date;

use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenCalendrier\Entity\Db\Calendrier;
use UnicaenCalendrier\Entity\Db\Date;
use UnicaenCalendrier\Entity\Db\DateType;

class DateService {
    use ProvidesObjectManager;

    /** Gestion des entités *******************************************************************************************/

    public function create(Date $date): Date
    {
        $this->getObjectManager()->persist($date);
        $this->getObjectManager()->flush($date);
        return $date;
    }

    public function update(Date $date): Date
    {
        $this->getObjectManager()->flush($date);
        return $date;
    }

    public function historise(Date $date): Date
    {
        $date->historiser();
        $this->getObjectManager()->flush($date);
        return $date;
    }

    public function restore(Date $date): Date
    {
        $date->dehistoriser();
        $this->getObjectManager()->flush($date);
        return $date;
    }

    public function delete(Date $date): Date
    {
        $this->getObjectManager()->remove($date);
        $this->getObjectManager()->flush($date);
        return $date;
    }

    /** Requetage *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Date::class)->createQueryBuilder('date')
            ->leftJoin('date.calendriers','calendrier')->addSelect('calendrier')
            ->leftJoin('date.type','dtype')->addSelect('dtype')
        ;
        return $qb;
    }

    /** @return Date[] */
    public function getDates(string $champ='debut', string $ordre='ASC', bool $withHisto = false): array
    {
        $qb= $this->createQueryBuilder()
            ->orderBy('date.' . $champ, $ordre);
        if (!$withHisto) $qb = $qb->andWhere('date.histoDestruction IS NULL');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getDate(?int $id): ?Date
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('date.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".Date::class."] partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    public function getRequestedDate(AbstractActionController $controller, string $params = 'date'): ?Date
    {
        $id = $controller->params()->fromRoute($params);
        return $this->getDate($id);
    }

    // TODO getWithCalendrier
    // TODO getWithType

    /** Facade ********************************************************************************************************/

    public function createWith(Calendrier $calendrier, DateType $type, DateTime $debut, ?DateTime $fin): Date
    {
        $date = new Date();
        $date->setCalendrier($calendrier);
        $date->setType($type);
        $date->setDebut($debut);
        $date->setFin($fin);
        $this->create($date);
        return $date;
    }
}