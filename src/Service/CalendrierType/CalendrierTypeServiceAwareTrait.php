<?php

namespace UnicaenCalendrier\Service\CalendrierType;

trait CalendrierTypeServiceAwareTrait {

    private CalendrierTypeService $calendrierTypeService;

    public function getCalendrierTypeService(): CalendrierTypeService
    {
        return $this->calendrierTypeService;
    }

    public function setCalendrierTypeService(CalendrierTypeService $calendrierTypeService): void
    {
        $this->calendrierTypeService = $calendrierTypeService;
    }

}