<?php

namespace UnicaenCalendrier\Service\CalendrierType;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenCalendrier\Entity\Db\CalendrierType;

class CalendrierTypeService {
    use ProvidesObjectManager;

    /** Gestion des entités *******************************************************************************************/

    public function create(CalendrierType $calendrierType): CalendrierType
    {
        $this->getObjectManager()->persist($calendrierType);
        $this->getObjectManager()->flush($calendrierType);
        return $calendrierType;
    }

    public function update(CalendrierType $calendrierType): CalendrierType
    {
        $this->getObjectManager()->flush($calendrierType);
        return $calendrierType;
    }

    public function delete(CalendrierType $calendrierType): CalendrierType
    {
        $this->getObjectManager()->remove($calendrierType);
        $this->getObjectManager()->flush($calendrierType);
        return $calendrierType;
    }

    /** Requetage *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(CalendrierType::class)->createQueryBuilder('calendrierType')
        ;
        return $qb;
    }

    /** @return CalendrierType[] */
    public function getCalendriersTypes(string $champ='libelle', string $ordre='ASC'): array
    {
        $qb= $this->createQueryBuilder()
            ->orderBy('calendrierType.' . $champ, $ordre);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getCalendrierType(?int $id): ?CalendrierType
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('calendrierType.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".CalendrierType::class."] partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    public function getRequestedCalendrierType(AbstractActionController $controller, string $param='calendrier-type'): ?CalendrierType
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getCalendrierType($id);
    }

    /** @return string[] */
    public function getCalendriersTypesAsOptions(): array
    {
        $types = $this->getCalendriersTypes();
        $options = [];
        foreach ($types as $type) {
            $options[$type->getId()] = $type->getLibelle();
        }
        return $options;
    }

    /** Facade ********************************************************************************************************/


}