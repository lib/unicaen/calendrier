UnicaenCalendrier
===

Description
---

Bibliothèque de gestion de dates et de calendriers. 
Cette bibliothèque permet de déclarer avec des interfaces de gestion des :
1. types de dates et des dates
2. type de calendriers et des calendriers

Aides de vue
---

*Affichage de calendriere `date`*

L'aide de vue `date` permet d'afficher une date.
Cette aide de vue prend trop paramètre

```php
echo $this->date($date, $calendrier, ['showAction' => true]);
```

*Affichage de calendriere `calendrier`* 


**TODOES**

- vérification dates requises vs dates fournies
- Précedences entre date
