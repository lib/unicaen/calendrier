<?php

namespace UnicaenCalendrier;

use Laminas\Router\Http\Literal;
use UnicaenCalendrier\Controller\IndexController;
use UnicaenCalendrier\Controller\IndexControllerFactory;
use UnicaenCalendrier\Provider\Privilege\UnicaencalendrierIndexPrivileges;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        UnicaencalendrierIndexPrivileges::INDEX,
                    ],
                    'roles' => []
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-calendrier' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/unicaen-calendrier',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            'unicaen-calendrier' => [
                                'label' => 'Gestion des calendriers',
                                'route' => 'unicaen-calendrier',
                                'resource' => PrivilegeController::getResourceId(IndexController::class, 'index'),
                                'order' => 5000,
                                'icon' => 'fas fa-angle-right',
                                'pages' => [
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ]

];