<?php

namespace UnicaenCalendrier;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenCalendrier\Controller\DateController;
use UnicaenCalendrier\Controller\DateControllerFactory;
use UnicaenCalendrier\Form\Date\DateForm;
use UnicaenCalendrier\Form\Date\DateFormFactory;
use UnicaenCalendrier\Form\Date\DateHydrator;
use UnicaenCalendrier\Form\Date\DateHydratorFactory;
use UnicaenCalendrier\Provider\Privilege\DatePrivileges;
use UnicaenCalendrier\Service\Date\DateService;
use UnicaenCalendrier\Service\Date\DateServiceFactory;
use UnicaenCalendrier\View\Helper\DateViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => DateController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DatePrivileges::DATE_INDEX
                    ],
                ],
                [
                    'controller' => DateController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        DatePrivileges::DATE_AFFICHER
                    ],
                ],
                [
                    'controller' => DateController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        DatePrivileges::DATE_AJOUTER
                    ],
                ],
                [
                    'controller' => DateController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        DatePrivileges::DATE_MODIFIER
                    ],
                ],
                [
                    'controller' => DateController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        DatePrivileges::DATE_HISTORISER
                    ],
                ],
                [
                    'controller' => DateController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        DatePrivileges::DATE_SUPPRIMER
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-calendrier' => [
                'child_routes' => [
                    'date' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/date',
                            'defaults' => [
                                /** @see DateController::indexAction() */
                                'controller' => DateController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:date',
                                    'defaults' => [
                                        /** @see DateController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see DateController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:date',
                                    'defaults' => [
                                        /** @see DateController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:date',
                                    'defaults' => [
                                        /** @see DateController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:date',
                                    'defaults' => [
                                        /** @see DateController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:date',
                                    'defaults' => [
                                        /** @see DateController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            DateService::class => DateServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            DateController::class => DateControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            DateForm::class => DateFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            DateHydrator::class => DateHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'date' => DateViewHelper::class,
        ],
    ],

];