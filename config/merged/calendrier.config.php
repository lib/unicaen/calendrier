<?php

namespace UnicaenCalendrier;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenCalendrier\Controller\CalendrierController;
use UnicaenCalendrier\Controller\CalendrierControllerFactory;
use UnicaenCalendrier\Form\Calendrier\CalendrierForm;
use UnicaenCalendrier\Form\Calendrier\CalendrierFormFactory;
use UnicaenCalendrier\Form\Calendrier\CalendrierHydrator;
use UnicaenCalendrier\Form\Calendrier\CalendrierHydratorFactory;
use UnicaenCalendrier\Provider\Privilege\CalendrierPrivileges;
use UnicaenCalendrier\Service\Calendrier\CalendrierService;
use UnicaenCalendrier\Service\Calendrier\CalendrierServiceFactory;
use UnicaenCalendrier\View\Helper\CalendrierViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => CalendrierController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        CalendrierPrivileges::CALENDRIER_INDEX
                    ],
                ],
                [
                    'controller' => CalendrierController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        CalendrierPrivileges::CALENDRIER_AFFICHER
                    ],
                ],
                [
                    'controller' => CalendrierController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        CalendrierPrivileges::CALENDRIER_AJOUTER
                    ],
                ],
                [
                    'controller' => CalendrierController::class,
                    'action' => [
                        'modifier',
                        'modifier-libelle',
                        'ajouter-date',
                    ],
                    'privileges' => [
                        CalendrierPrivileges::CALENDRIER_MODIFIER
                    ],
                ],
                [
                    'controller' => CalendrierController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        CalendrierPrivileges::CALENDRIER_HISTORISER
                    ],
                ],
                [
                    'controller' => CalendrierController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        CalendrierPrivileges::CALENDRIER_SUPPRIMER
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-calendrier' => [
                'child_routes' => [
                    'calendrier' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/calendrier',
                            'defaults' => [
                                /** @see CalendrierController::indexAction() */
                                'controller' => CalendrierController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:calendrier',
                                    'defaults' => [
                                        /** @see CalendrierController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see CalendrierController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:calendrier',
                                    'defaults' => [
                                        /** @see CalendrierController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:calendrier',
                                    'defaults' => [
                                        /** @see CalendrierController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:calendrier',
                                    'defaults' => [
                                        /** @see CalendrierController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:calendrier',
                                    'defaults' => [
                                        /** @see CalendrierController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'ajouter-date' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter-date/:calendrier',
                                    'defaults' => [
                                        /** @see CalendrierController::ajouterDateAction() */
                                        'action' => 'ajouter-date',
                                    ],
                                ],
                            ],
                            'modifier-libelle' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier-libelle/:calendrier',
                                    'defaults' => [
                                        /** @see CalendrierController::modifierLibelleAction() */
                                        'action' => 'modifier-libelle',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            CalendrierService::class => CalendrierServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            CalendrierController::class => CalendrierControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            CalendrierForm::class => CalendrierFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            CalendrierHydrator::class => CalendrierHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'calendrier' => CalendrierViewHelper::class,
        ],
    ],

];