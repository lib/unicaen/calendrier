<?php

namespace UnicaenCalendrier;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenCalendrier\Controller\CalendrierTypeController;
use UnicaenCalendrier\Controller\CalendrierTypeControllerFactory;
use UnicaenCalendrier\Form\CalendrierType\CalendrierTypeForm;
use UnicaenCalendrier\Form\CalendrierType\CalendrierTypeFormFactory;
use UnicaenCalendrier\Form\CalendrierType\CalendrierTypeHydrator;
use UnicaenCalendrier\Form\CalendrierType\CalendrierTypeHydratorFactory;
use UnicaenCalendrier\Provider\Privilege\CalendriertypePrivileges;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeServiceFactory;
use UnicaenCalendrier\Service\CalendrierType\CalendrierTypeService;
use UnicaenPrivilege\Guard\PrivilegeController;



return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => CalendrierTypeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        CalendriertypePrivileges::CALENDRIERTYPE_INDEX
                    ],
                ],
                [
                    'controller' => CalendrierTypeController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        CalendriertypePrivileges::CALENDRIERTYPE_AFFICHER
                    ],
                ],
                [
                    'controller' => CalendrierTypeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        CalendriertypePrivileges::CALENDRIERTYPE_AJOUTER
                    ],
                ],
                [
                    'controller' => CalendrierTypeController::class,
                    'action' => [
                        'modifier',
                        'gerer-dates-types',
                    ],
                    'privileges' => [
                        CalendriertypePrivileges::CALENDRIERTYPE_MODIFIER
                    ],
                ],
                [
                    'controller' => CalendrierTypeController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        CalendriertypePrivileges::CALENDRIERTYPE_SUPPRIMER
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-calendrier' => [
                'child_routes' => [
                    'calendrier-type' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/calendrier-type',
                            'defaults' => [
                                /** @see CalendrierTypeController::indexAction() */
                                'controller' => CalendrierTypeController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:calendrier-type',
                                    'defaults' => [
                                        /** @see CalendrierTypeController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see CalendrierTypeController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:calendrier-type',
                                    'defaults' => [
                                        /** @see CalendrierTypeController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'gerer-dates-types' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/gerer-dates-types/:calendrier-type',
                                    'defaults' => [
                                        /** @see CalendrierTypeController::gererDatesTypesAction() */
                                        'action' => 'gerer-dates-types',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:calendrier-type',
                                    'defaults' => [
                                        /** @see CalendrierTypeController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            CalendrierTypeService::class => CalendrierTypeServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            CalendrierTypeController::class => CalendrierTypeControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            CalendrierTypeForm::class => CalendrierTypeFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            CalendrierTypeHydrator::class => CalendrierTypeHydratorFactory::class,
        ],
    ],

];