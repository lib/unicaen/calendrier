<?php

namespace UnicaenCalendrier;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenCalendrier\Controller\DateTypeController;
use UnicaenCalendrier\Controller\DateTypeControllerFactory;
use UnicaenCalendrier\Form\DateType\DateTypeForm;
use UnicaenCalendrier\Form\DateType\DateTypeFormFactory;
use UnicaenCalendrier\Form\DateType\DateTypeHydrator;
use UnicaenCalendrier\Form\DateType\DateTypeHydratorFactory;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesForm;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesFormFactory;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesHydrator;
use UnicaenCalendrier\Form\SelectionnerDatesTypes\SelectionnerDatesTypesHydratorFactory;
use UnicaenCalendrier\Provider\Privilege\DatetypePrivileges;
use UnicaenCalendrier\Service\DateType\DateTypeService;
use UnicaenCalendrier\Service\DateType\DateTypeServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => DateTypeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        DatetypePrivileges::DATETYPE_INDEX
                    ],
                ],
                [
                    'controller' => DateTypeController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        DatetypePrivileges::DATETYPE_AFFICHER
                    ],
                ],
                [
                    'controller' => DateTypeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        DatetypePrivileges::DATETYPE_AJOUTER
                    ],
                ],
                [
                    'controller' => DateTypeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        DatetypePrivileges::DATETYPE_MODIFIER
                    ],
                ],
                [
                    'controller' => DateTypeController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        DatetypePrivileges::DATETYPE_SUPPRIMER
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-calendrier' => [
                'child_routes' => [
                    'date-type' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/date-type',
                            'defaults' => [
                                /** @see DateTypeController::indexAction() */
                                'controller' => DateTypeController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:date-type',
                                    'defaults' => [
                                        /** @see DateTypeController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see DateTypeController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:date-type',
                                    'defaults' => [
                                        /** @see DateTypeController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:date-type',
                                    'defaults' => [
                                        /** @see DateTypeController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            DateTypeService::class => DateTypeServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            DateTypeController::class => DateTypeControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            DateTypeForm::class => DateTypeFormFactory::class,
            SelectionnerDatesTypesForm::class => SelectionnerDatesTypesFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            DateTypeHydrator::class => DateTypeHydratorFactory::class,
            SelectionnerDatesTypesHydrator::class => SelectionnerDatesTypesHydratorFactory::class,
        ],
    ],

];