<?php

use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use UnicaenCalendrier\Form\ModifierLibelle\ModifierLibelleForm;
use UnicaenCalendrier\Form\ModifierLibelle\ModifierLibelleFormFactory;
use UnicaenCalendrier\Form\ModifierLibelle\ModifierLibelleHydrator;
use UnicaenCalendrier\Form\ModifierLibelle\ModifierLibelleHydratorFactory;

return [
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenCalendrier\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'UNICAEN-CALENDRIER__' . __NAMESPACE__,
            ],
        ],
    ],


    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'form_elements' => [
        'factories' => [
            ModifierLibelleForm::class => ModifierLibelleFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            ModifierLibelleHydrator::class => ModifierLibelleHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'aliases' => [
        ],
        'factories' => [
        ],
    ],
    'public_files' => [
        'stylesheets' => [
            '10000_calendrier' => 'css/unicaen-calendrier.css',
        ],
    ],
];